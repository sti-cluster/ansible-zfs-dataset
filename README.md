
Role ZFS Dataset
=========

Ansible role to install silently ZFS 6.8 with licenses.

Compatibility
------------

This role can be used on Ubuntu 18. Other OS will come later

Variables Used
------------

You can find in defaults/main.yml all variables used in tasks

| Variable                 | Default Value                                                | Type   | Description                                                  |
| ------------------------ | :----------------------------------------------------------- | :----- | ------------------------------------------------------------ |
| fstype     | ZFS    | String | File System                                                    |
| dev   | /dev/nvme0n1                                                       | String | Default Device for ZFS Dataset                         |
| mount          | /home                                                            | String   | Default mountpoint |
| opts                      | rw                                                   | String | Default access Read / write                          |
| state                 | mounted                                                            | String | Default mount state                 |

## Author Information

Written by [Pullyvan Krishnamoorthy ](mailto:pullyvan.krishnamoorthy@epfl.ch) for EPFL - STI school of engineering
=======
